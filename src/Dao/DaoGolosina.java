package Dao;

import Librerias.Validaciones;
import Modelo.ClassConexionDAO;
import Modelo.Golosina;
import java.sql.ResultSet;

public class DaoGolosina extends ClassConexionDAO
{
  public void Insertar(Golosina golosina)  
  {
    String Sql="INSERT INTO golosina(codigo, nombre, cantidad, precio) VALUES (";
    Sql=Sql+Validaciones.Apost(golosina.getCodigo())+",";
    Sql=Sql+Validaciones.Apost(golosina.getNombre())+",";
    Sql=Sql+Validaciones.Apost(Integer.toString(golosina.getCantidad()))+",";
    Sql=Sql+Validaciones.Apost(Double.toString(golosina.getPrecio()))+")";

   PackageConeccion.ConeccionBD.ejecutar(Sql);
  }   
 //--------------------------------------
public ResultSet BuscarGolo(String codigo)  
{
  ResultSet registro;
  
  String Sql="SELECT * FROM golosina WHERE codigo="+Validaciones.Apost(codigo);
  
  registro=PackageConeccion.ConeccionBD.consultar(Sql);
  return registro;
}        
//------------------------
public ResultSet CargarGolo()  
{
  ResultSet registro;
  
  String Sql="SELECT * FROM golosina"; 
  
  registro=PackageConeccion.ConeccionBD.consultar(Sql);
  return registro;
}        

}
