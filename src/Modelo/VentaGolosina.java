/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author usuario
 */
public class VentaGolosina {
    int codigo;
    String cedula_cliente,cedula_empleado;
    Double acum_precio;

    public VentaGolosina(int codigo, String cedula_cliente, String cedula_empleado) {
        this.codigo = codigo;
        this.cedula_cliente = cedula_cliente;
        this.cedula_empleado = cedula_empleado;
        this.acum_precio = 0d;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCedula_cliente() {
        return cedula_cliente;
    }

    public void setCedula_cliente(String cedula_cliente) {
        this.cedula_cliente = cedula_cliente;
    }

    public String getCedula_empleado() {
        return cedula_empleado;
    }

    public void setCedula_empleado(String cedula_empleado) {
        this.cedula_empleado = cedula_empleado;
    }

    public Double getAcum_precio() {
        return acum_precio;
    }

   public void acumPrecio(double precio,int cant){
   acum_precio += precio*cant;}
   
    
}
