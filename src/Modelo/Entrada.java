/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author usuario
 */
class Entrada {
   private int codigo,codigofuncion,codigo_venta_entrada;
   private Date fecha;
   private TipoEntrada tipoentrada;
   private Sucursal suc;

    public Entrada(int codigo, int codigofuncion, int codigo_venta_entrada, Date fecha, TipoEntrada tipoentrada, Sucursal suc) {
        this.codigo = codigo;
        this.codigofuncion = codigofuncion;
        this.codigo_venta_entrada = codigo_venta_entrada;
        this.fecha = fecha;
        this.tipoentrada = tipoentrada;
        this.suc = suc;
    }

    
    public Sucursal getSuc() {
        return suc;
    }

    public void setSuc(Sucursal suc) {
        this.suc = suc;
    }
       

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigofuncion() {
        return codigofuncion;
    }

    public void setCodigofuncion(int codigofuncion) {
        this.codigofuncion = codigofuncion;
    }

    public int getCodigo_venta_entrada() {
        return codigo_venta_entrada;
    }

    public void setCodigo_venta_entrada(int codigo_venta_entrada) {
        this.codigo_venta_entrada = codigo_venta_entrada;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TipoEntrada getTipoentrada() {
        return tipoentrada;
    }

    public void setTipoentrada(TipoEntrada tipoentrada) {
        this.tipoentrada = tipoentrada;
    }
    public double CalcularPrecio() {
        double precio = suc.getPrecio_general();
        DayOfWeek hoy = LocalDate.now().getDayOfWeek();
             
       //se revisa la edad
       switch(tipoentrada){
           
           case DE1A4:
               precio -= suc.getPrecio_general()*suc.getDesc1a4();
               precio = (hoy == DayOfWeek.MONDAY?precio/2:precio); //mitad de precio si es lunes
               break;
           case DE5A12:
               precio -= suc.getPrecio_general()*suc.getDesc5a12();
               precio = (hoy == DayOfWeek.MONDAY?precio/2:precio);//mitad de precio si es lunes
               break;
           case DE13A59:
              precio -= suc.getPrecio_general()*suc.getDesc13a59();
              precio = (hoy == DayOfWeek.MONDAY?precio/2:precio);//mitad de precio si es lunes
               break;
           case MASDE60:
              precio = suc.getPrecio_general()*suc.getDesc13a59()/2;
              break;
           default:
               precio = 0;
               break;
             }
       
       if(hoy == DayOfWeek.SATURDAY || hoy == DayOfWeek.SUNDAY || suc.isFeriado())
            precio *= 1.3; //30% adicional si es fin de semana o feriado
        
        return precio;
                
    }

}
