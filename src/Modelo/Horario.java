/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.sql.Time;

/**
 *
 * @author usuario
 */
class Horario {
   private int codigo;
   private Time descripcion;

    public Horario(int codigo, Time descripcion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
       
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public TipoHorario Tipoh() {
       if(descripcion.after(Time.valueOf("19:00:00")))  
           return TipoHorario.NOCTURNO;
        else
           return TipoHorario.DIURNO;
        
    }

  
    public Time getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(Time descripcion) {
        this.descripcion = descripcion;
    }
}
