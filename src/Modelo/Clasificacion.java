/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author usuario
 */
public enum Clasificacion {
   A (true),B(true),C(false),D(false) ;
   private final boolean tiene_diurno;

    private Clasificacion(boolean tiene_diurno) {
        this.tiene_diurno = tiene_diurno;
    }
   public boolean tieneDiurno(){
   return tiene_diurno;}
}
