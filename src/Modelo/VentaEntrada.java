/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class VentaEntrada {
    ArrayList<Entrada> entradas_compradas;
    int codigo;
    String cedula_cliente,cedula_empleado;

    public VentaEntrada(ArrayList<Entrada> entradas_compradas, int codigo, String cedula_cliente, String cedula_empleado) {
        this.entradas_compradas = entradas_compradas;
        this.codigo = codigo;
        this.cedula_cliente = cedula_cliente;
        this.cedula_empleado = cedula_empleado;
    }

    public String getCedula_cliente() {
        return cedula_cliente;
    }

    public void setCedula_cliente(String cedula_cliente) {
        this.cedula_cliente = cedula_cliente;
    }

    public String getCedula_empleado() {
        return cedula_empleado;
    }

    public void setCedula_empleado(String cedula_empleado) {
        this.cedula_empleado = cedula_empleado;
    }


    public ArrayList<Entrada> getEntradas_compradas() {
        return entradas_compradas;
    }

    public void setEntradas_compradas(ArrayList<Entrada> entradas_compradas) {
        this.entradas_compradas = entradas_compradas;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

       
    public double generarMontoTotal(){
        double total = 0d;
       for(Entrada le_ec:entradas_compradas){
       total += le_ec.CalcularPrecio();
       }
        return total;}
            
}
