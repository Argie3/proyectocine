/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Date;

/**
 *
 * @author usuario
 */
public class Funcion {
  private int codigo,codigo_pelicula,codigo_sala;
  private Horario horario;

    public Funcion(int codigo, int codigo_pelicula, int codigo_sala, Horario horario) {
        this.codigo = codigo;
        this.codigo_pelicula = codigo_pelicula;
        this.codigo_sala = codigo_sala;
        this.horario = horario;
    }

    
   public int getCodigo_sala() {
        return codigo_sala;
    }

    public void setCodigo_sala(int codigo_sala) {
        this.codigo_sala = codigo_sala;
    }

     public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo_pelicula() {
        return codigo_pelicula;
    }

    public void setCodigo_pelicula(int codigo_pelicula) {
        this.codigo_pelicula = codigo_pelicula;
    }

   
    public Horario getHorario() {
        return horario;
    }

    public void setHorario(Horario horario) {
        this.horario = horario;
    }
}
