/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Librerias.Validaciones;
import java.time.LocalDate;
import java.util.ArrayList;

class contadorfrecuencia{
int cont;
String cedulacli;

    public contadorfrecuencia(String cedulacli) {
        this.cont = 0;
        this.cedulacli = cedulacli;
    }
    }
public class Sucursal {
   private double precio_general,codigo;
   private ArrayList<Empleado> a_empleados;
   private ArrayList<Pelicula> cartelera;
   private ArrayList<Sala> a_salas;
   private ArrayList<Funcion> a_funciones;
   private ArrayList<Horario> a_horarios;
   private double desc1a4,desc5a12,desc13a59,desc60;
   private boolean feriado;
   private String nombre;

    public Sucursal(double precio_general, double codigo, ArrayList<Empleado> a_empleados, ArrayList<Pelicula> cartelera, ArrayList<Sala> a_salas, ArrayList<Funcion> a_funciones, ArrayList<Horario> a_horarios, double desc1a4, double desc5a12, double desc13a59, double desc60, boolean feriado, String nombre) {
        this.precio_general = precio_general;
        this.codigo = codigo;
        this.a_empleados = a_empleados;
        this.cartelera = cartelera;
        this.a_salas = a_salas;
        this.a_funciones = a_funciones;
        this.a_horarios = a_horarios;
        this.desc1a4 = desc1a4;
        this.desc5a12 = desc5a12;
        this.desc13a59 = desc13a59;
        this.desc60 = desc60;
        this.feriado = feriado;
        this.nombre = nombre;
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

   
    public ArrayList<Horario> getA_horarios() {
        return a_horarios;
    }

    public void setA_horarios(ArrayList<Horario> a_horarios) {
        this.a_horarios = a_horarios;
    }
       
    public double getDesc1a4() {
        return desc1a4;
    }

    public void setDesc1a4(double desc1a4) {
        this.desc1a4 = desc1a4;
    }

    public double getDesc5a12() {
        return desc5a12;
    }

    public void setDesc5a12(double desc5a12) {
        this.desc5a12 = desc5a12;
    }

    public double getDesc13a59() {
        return desc13a59;
    }

    public void setDesc13a59(double desc13a59) {
        this.desc13a59 = desc13a59;
    }

    public boolean isFeriado() {
        return feriado;
    }

    public void setFeriado(boolean feriado) {
        this.feriado = feriado;
    }

    public double getDesc60() {
        return desc60;
    }

    public void setDesc60(double desc60) {
        this.desc60 = desc60;
    }
   
    

    public double getPrecio_general() {
        return precio_general;
    }

    public void setPrecio_general(double precio_general) {
        this.precio_general = precio_general;
    }

    public double getCodigo() {
        return codigo;
    }

    public void setCodigo(double codigo) {
        this.codigo = codigo;
    }

    public ArrayList<Empleado> getA_empleados() {
        return a_empleados;
    }

    public void setA_empleados(ArrayList<Empleado> a_empleados) {
        this.a_empleados = a_empleados;
    }

    public ArrayList<Pelicula> getCartelera() {
        return cartelera;
    }

    public void setCartelera(ArrayList<Pelicula> cartelera) {
        this.cartelera = cartelera;
    }

    public ArrayList<Sala> getA_salas() {
        return a_salas;
    }

    public void setA_salas(ArrayList<Sala> a_salas) {
        this.a_salas = a_salas;
    }

    public ArrayList<Funcion> getA_funciones() {
        return a_funciones;
    }

    public void setA_funciones(ArrayList<Funcion> a_funciones) {
        this.a_funciones = a_funciones;
    }
   
   public String ClienteFrecuente(ArrayList<VentaEntrada> aventae,int criterio){
     //crea un simple arreglo de struct que solo contiene la cedula y un contador de veces qeu fue al cine
     boolean esta_en_el_arreglo;
     ArrayList<contadorfrecuencia> a_cf = new ArrayList<contadorfrecuencia>();
         //revisa todas las entradas
     for(VentaEntrada le_ve:aventae){
     //la primera entrada la agrega al arreglo
     if(a_cf.isEmpty())
         a_cf.add(new contadorfrecuencia(le_ve.getCedula_cliente()));
     else{ 
         //si vuelve aparecer en la siguiente la cuenta
          esta_en_el_arreglo = false;
         for(contadorfrecuencia le_cf:a_cf){
             if(le_cf.cedulacli.equals(le_ve.getCedula_cliente())){
             le_cf.cont++;
             esta_en_el_arreglo = true;
             break;
            }
         }
      //si aparece otra diferente se agrega en el arreglo
      if(!esta_en_el_arreglo)
      a_cf.add(new contadorfrecuencia(le_ve.getCedula_cliente()));
      
     }
     }
         
     //Se revisa el arreglo del struct 
     //y se ve cual es el numero mas grande de los contadores basados en el criterio
   ArrayList<String> a_ceds = new ArrayList<String>();
     for(contadorfrecuencia le_cf1:a_cf){
  // si cumple el criterio, se meten en el arreglo de cedulas
        if(le_cf1.cont >= criterio)
       a_ceds.add(le_cf1.cedulacli);
    }
     if(a_ceds.isEmpty())
         return "no hay cliente frecuente";
     //si el arreglo esta vacio devuelce un mensaje
     else if(a_ceds.size() ==1)
        //si tiene 1 devuelve ese nada mas
         return a_ceds.get(0);
     else
          //si tiene mas de 1. lo elige al azar
         return a_ceds.get(Validaciones.Generar_Aleatorio(a_ceds.size()));
    
          }
   public void GenerarFunciones(int cod_ultimfunc){
  // se prepara toda la semana
  LocalDate dia = LocalDate.now();
  for(int i = 1;i<7;i++){
   //se prepara cada una de las salas
   for(Sala le_sala:a_salas){
    //elige una pelicula al azar
   Pelicula le_peli = cartelera.get(Validaciones.Generar_Aleatorio(cartelera.size()));
    Horario le_horar;
   do{
   //elige un horario al azar
   le_horar = a_horarios.get(Validaciones.Generar_Aleatorio(a_horarios.size()));
   }while(!le_peli.getCensura().tieneDiurno()&&le_horar.Tipoh() == TipoHorario.DIURNO);
   cod_ultimfunc++;
   Funcion le_funcion = new Funcion(cod_ultimfunc,le_peli.getCodigo(),le_sala.getCodigo(),le_horar);
       a_funciones.add(le_funcion);
   }
   dia =dia.plusDays(1);
  }
   }
}
    


