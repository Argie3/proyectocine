/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Date;


public class Empleado extends Persona {
    Cargo cargo;
    float sueldo,acumgolosina_semanal;

    public Empleado(Cargo cargo, float sueldo, float acumgolosina_semanal, String nombre, String apellido, String ci, String direccion, String tlf, Date f_nac) {
        super(nombre, apellido, ci, direccion, tlf, f_nac);
        this.cargo = cargo;
        this.sueldo = sueldo;
        this.acumgolosina_semanal = acumgolosina_semanal;
    }

    public Empleado() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public float getSueldo() {
        return sueldo;
    }

    public void setSueldo(float sueldo) {
        this.sueldo = sueldo;
    }

    public float getAcumgolosina_semanal() {
        return acumgolosina_semanal;
    }

    public void setAcumgolosina_semanal(float acumgolosina_semanal) {
        this.acumgolosina_semanal = acumgolosina_semanal;
    }
    
}
