/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
/**
 *
 * @author usuario
 */
public class Persona {
   private String nombre;
   private String apellido;
   private String ci;
   private String direccion;
   private String tlf;
   private Date f_nac;

    public Persona(String nombre, String apellido, String ci, String direccion, String tlf, Date f_nac) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.ci = ci;
        this.direccion = direccion;
        this.tlf = tlf;
        this.f_nac = f_nac;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     * @return the ci
     */
    public String getCi() {
        return ci;
    }

    /**
     * @param ci the ci to set
     */
    public void setCi(String ci) {
        this.ci = ci;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the tlf
     */
    public String getTlf() {
        return tlf;
    }

    /**
     * @param tlf the tlf to set
     */
    public void setTlf(String tlf) {
        this.tlf = tlf;
    }

    /**
     * @return the f_nac
     */
    public Date getF_nac() {
        return f_nac;
    }

    /**
     * @param f_nac the f_nac to set
     */
    public void setF_nac(Date f_nac) {
        this.f_nac = f_nac;
    }
    public int CalcularEdad(){
    LocalDate hoy = LocalDate.now();
    LocalDate cumple = f_nac.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
     int anos = Period.between(hoy, cumple).getYears();
    return anos;}
}
