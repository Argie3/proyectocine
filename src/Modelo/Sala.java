/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author usuario
 */
public class Sala {
    int codigo,cant_asientos,nrosala;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCant_asientos() {
        return cant_asientos;
    }

    public void setCant_asientos(int cant_asientos) {
        this.cant_asientos = cant_asientos;
    }

    public int getNrosala() {
        return nrosala;
    }

    public void setNrosala(int nrosala) {
        this.nrosala = nrosala;
    }

    public Sala(int codigo, int cant_asientos, int nrosala) {
        this.codigo = codigo;
        this.cant_asientos = cant_asientos;
        this.nrosala = nrosala;
    }
   
}
