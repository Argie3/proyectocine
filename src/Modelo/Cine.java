/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;


public class Cine  {
  String nombre,direccion,telefono,ciudad;
  
  ArrayList<Sucursal> arreglo_sucursal; 
  ArrayList<Pelicula> arreglo_peliculas;
  ArrayList<Cliente> arreglo_clientes;

    public Cine(String nombre, String direccion, String telefono, String ciudad, ArrayList<Sucursal> arreglo_sucursal, ArrayList<Pelicula> arreglo_peliculas, ArrayList<Cliente> arreglo_clientes) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.ciudad = ciudad;
        this.arreglo_sucursal = arreglo_sucursal;
        this.arreglo_peliculas = arreglo_peliculas;
        this.arreglo_clientes = arreglo_clientes;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public ArrayList<Sucursal> getArreglo_sucursal() {
        return arreglo_sucursal;
    }

    public void setArreglo_sucursal(ArrayList<Sucursal> arreglo_sucursal) {
        this.arreglo_sucursal = arreglo_sucursal;
    }

    public ArrayList<Pelicula> getArreglo_peliculas() {
        return arreglo_peliculas;
    }

    public void setArreglo_peliculas(ArrayList<Pelicula> arreglo_peliculas) {
        this.arreglo_peliculas = arreglo_peliculas;
    }

    public ArrayList<Cliente> getArreglo_clientes() {
        return arreglo_clientes;
    }

    public void setArreglo_clientes(ArrayList<Cliente> arreglo_clientes) {
        this.arreglo_clientes = arreglo_clientes;
    }
  
  public void agregarSucursal(Sucursal le_sucur){
  arreglo_sucursal.add(le_sucur); }
  public void modificarSucursal(int cod_sucur,Sucursal le_sucur){
  arreglo_sucursal.set(cod_sucur, le_sucur);
  
  }
  public void agregarCliente(Cliente le_cliente){
  arreglo_clientes.add(le_cliente);}
  public void modificarCliente(int cod_cli,Cliente le_cli){
      arreglo_clientes.set(cod_cli, le_cli);
  }
  public void eliminarPelicula(int cod_peli){
  for(Pelicula le_peli:arreglo_peliculas){
  if(le_peli.getCodigo()== cod_peli){
  arreglo_peliculas.remove(cod_peli);
  break;
        }
     }
    }
  }
