/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author usuario
 */
public class Pelicula {
  private int codigo;
  private String nombre;
  private Clasificacion censura;

    public Pelicula(int codigo, String nombre, Clasificacion censura) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.censura = censura;
    }

    public Clasificacion getCensura() {
        return censura;
    }

    public void setCensura(Clasificacion censura) {
        this.censura = censura;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
   
  
}
