/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Modelo.Sucursal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vista.jCinesVenezuela;
/**
 *
 * @author Usuario
 */
public class CtrlCinesVenezuela implements ActionListener{
    private jCinesVenezuela jCV;
    
    public CtrlCinesVenezuela(){
        jCV = new jCinesVenezuela();
        jCV.setVisible(true);
        jCV.setResizable(false);
        jCV.agregarListener(this);
        
    }
   public void iniciar(){
        jCV.setLocationRelativeTo(null);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Sucursal sucur1 = null;
        if (e.getSource().equals(jCV.getMenuItemBQTO()))   
       {
            //aqui debe haber una chorrera de vainas de basa de datos e instanciar objetos
             new CtrlSucursal(sucur1);
       }
        if (e.getSource().equals(jCV.getMenuItemCaracas()))   
       {
            
           new CtrlSucursal(sucur1);
       }
        if (e.getSource().equals(jCV.getMenuItemFalcon()))   
       {
          new CtrlSucursal(sucur1);
        
    }
          if (e.getSource().equals(jCV.getMenuItemMerida()))   
       {
          
        new CtrlSucursal(sucur1);
    }
         if (e.getSource().equals(jCV.getMenuItemIngresoP()))   
       {
          new CtrlIngresoPeliculas();
        
    }
          if (e.getSource().equals(jCV.getMenuItemAgregar()))   
       {
         new CtrlSucursalAg();
        
    }
           if (e.getSource().equals(jCV.getMenuItemSalir()))   
       {
          
        jCV.dispose();
    }
         if (e.getSource().equals(jCV.getMenuItemClientes()))   
       {
          new CtrlClientes();
        
    }
        if (e.getSource().equals(jCV.getMenuItemEdades()))   
       {
          
        new CtrlPEdades();
    }
         if (e.getSource().equals(jCV.getMenuItemPeliMenorV()))   
       {
          new CtrlPeliculaMenorV();
       
    }
         if (e.getSource().equals(jCV.getMenuItemPeliculas()))   
       {
          new CtrlPelicula();
       
    }
    
}}
