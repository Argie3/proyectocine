/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Modelo.Sucursal;
import Vista.jSucursal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 *
 * @author Usuario
 */
public class CtrlSucursal implements ActionListener{
    private jSucursal jS;
    private Sucursal sucursal;
    public CtrlSucursal(Sucursal sucursal){
        jS= new jSucursal();
        jS.agregarListener(this);
        jS.setResizable(false);
        jS.setVisible(true);
        this.sucursal = sucursal;
        jS.getjLabel2().setText(sucursal.getNombre());
      
    }
    
 public void iniciar(){
        jS.setLocationRelativeTo(null);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(jS.getjMenuItemCFrecuente()))   
       {
             new CtrlClienteFrecuente();
       }
        if (e.getSource().equals(jS.getjMenuItemCGolosina()))   
       {
             new CtrlGolosina();
       }
       
         if (e.getSource().equals(jS.getjMenuItemDisponibilidad()))   
       {
             new CtrlDisponibilidad();
       }
       
           if (e.getSource().equals(jS.getjMenuItemEmpleados()))   
       {
             new CtrlEmpleados();
       }
           if (e.getSource().equals(jS.getjMenuItemFunciones()))   
       {
             new CtrlFunciones();
       }
            if (e.getSource().equals(jS.getjMenuItemGFunciones()))   
       {
             new CtrlGenerarFunciones();
       }
             if (e.getSource().equals(jS.getjMenuItemGolosinas()))   
       {
             new CtrlGolosina();
       }
        if (e.getSource().equals(jS.getjMenuItemHorarios()))   
       {
             new CtrlHorario();
       }
       
          if (e.getSource().equals(jS.getjMenuItemMontoE()))   
       {
             new CtrljMontoEmpleG();
       }
       
            if (e.getSource().equals(jS.getjMenuItemPeliMes()))   
       {
             new CtrlPeliculasMes();
       }
             if (e.getSource().equals(jS.getjMenuItemPeliS()))   
       {
             new CtrlPeliculaSala();
       }
               if (e.getSource().equals(jS.getjMenuItemSalas()))   
       {
             new CtrlSala();
       }
                if (e.getSource().equals(jS.getjMenuItemSalir()))   
       {
             jS.dispose();
       }
                 if (e.getSource().equals(jS.getjMenuItemVEntrada()))   
       {
             new CtrlVentaEntrada();
       }
                  if (e.getSource().equals(jS.getjMenuItemVGolosina()))   
       {
             new CtrlVentaGolosina();
       }
    }
    
}
