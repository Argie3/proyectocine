/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Librerias.Validaciones;
import Vista.jTicketG;
import java.awt.event.ActionEvent;

/**
 *
 * @author Usuario
 */
public class CtrlTicketG {
    private jTicketG jTg;
     public CtrlTicketG(long ticket,
                             String Nombre,
                             String Cedula, 
                             String Sub, 
                             String total, 
                             String Sucursal){
         
        jTg = new jTicketG();
        jTg.setVisible(true);
     
        jTg.getjTextFieldCodigo().setText("001"+String.valueOf(ticket));
        jTg.getjTextFieldFecha().setText(Validaciones.getFechaActual());
        jTg.getjTextFieldNombre().setText(Nombre);
        jTg.getjTextFieldCedula().setText(Cedula);
        jTg.getjTextFieldTotal().setText(total);
        jTg.getjTextFieldSub().setText(Sub);
        jTg.getjTextFieldSucursal().setText(Sucursal);
     }

    CtrlTicketG() {
       
    }

    
    
}
