/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;
import Vista.jTicketEspecial;
import Librerias.Validaciones;
/**
 *
 * @author Usuario
 */
public class CtrlTicketEspecial {
    private jTicketEspecial jTe;
   public CtrlTicketEspecial(long ticket,
                             String Nombre,
                             String Cedula){
      jTe = new jTicketEspecial();
      jTe.setVisible(true);
      jTe.setResizable(false);
     
        jTe.getjTextFieldCodigo().setText("001"+String.valueOf(ticket));
        jTe.getjTextFieldFecha().setText(Validaciones.getFechaActual());
        jTe.getjTextFieldNombre().setText(Nombre);
        jTe.getjTextFieldCedula().setText(Cedula);
     }
   public CtrlTicketEspecial(){
       
   }
}
